<?php

$options = getopt('l');
if (isset($options['l'])) {
  // l: Local loading, for build testing.
  include_once __DIR__ . '/../../lib/autoload.php';
}
else {
  // Usual loading, e.g. at autopkgtest run.
  include_once 'EasyRdf/autoload.php';
}
$url = 'http://localhost:10101/foaf.rdf';
#$url = 'http://njh.me/foaf.rdf';
$foaf = new \EasyRdf\Graph($url);
$foaf->load();
$author = $foaf->primaryTopic();
echo 'Main author name is: ', $author->get('foaf:name'), PHP_EOL;
