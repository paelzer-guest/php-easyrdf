#!/bin/sh

set -e

# Start a web server to serve the file.
php --server localhost:10101 --docroot debian/tests/data &
SERVER_PID=$!

# A graphviz source file can be created.
php debian/tests/rapper.php > sample.dot

kill -9 $SERVER_PID

# A valid dot can be used by graphviz.
dot -Tpng sample.dot > sample.png
MIME_TYPE=`file --mime-type --brief sample.png`
if [ "$MIME_TYPE" != 'image/png' ]
then
    exit 1;
fi
